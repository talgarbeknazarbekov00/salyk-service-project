package com.example.thymeleafspringboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class PeopleController {


    @Autowired
    private EmailSenderService emailSenderService;
    @Autowired
    private PeopleRepository repository;
    @Autowired
    private PeopleServiceImpl peopleService;

    @GetMapping("/start")
    public String view(Model model){
        return getAll(model,1);
    }
    @GetMapping("/list/{pageNumber}")
    public String getAll(Model model,@PathVariable int pageNumber){
        int size = 2;
        Page<People> page = peopleService.paginationGet(pageNumber,size);
        model.addAttribute("totalPage",page.getTotalPages());
        model.addAttribute("list", page.getContent());
        model.addAttribute("totalItem",page.getTotalElements());
        model.addAttribute("currentPage",pageNumber);
        return "index";
    }

    @GetMapping("/new")
    public String newPeople(Model model) {
        model.addAttribute("people", new People());
        return "registration";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute People people) {
        repository.save(people);
        SendEmail(people.getEmail(),people.getName(),"вы успешно добавлены");
        return "redirect:start";
    }
    public void SendEmail (String toEmail,String subject,String body){
        emailSenderService.sendEmail(toEmail, subject,body);
    }

    @GetMapping("/getById/{id}")
    public String getById(Model model,@PathVariable long id){
        model.addAttribute("peoples", repository.getById(id));
        return "index";
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable long id) {
        repository.deleteById(id);
        return "redirect:/start";
    }

    @GetMapping("update/{id}")
    public String getCompanyById(Model model, @PathVariable long id) {
        model.addAttribute("currentPeople", repository.getById(id));
        return "updatePage";
    }

    @PostMapping("/saveUpdated/{id}")
    public String update(@ModelAttribute People currentPeople,@PathVariable long id) {
        People people = repository.getById(id);
        people.setName(currentPeople.getName());
        people.setEmail(currentPeople.getEmail());
        people.setPassword(currentPeople.getPassword());
        repository.save(people);
        return "redirect:/start";
    }
}
