package com.example.thymeleafspringboot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PeopleRepository extends JpaRepository<People,Long> {

    @Query("delete from People where id=?1")
    void deletePeopleById(long id);
}
