package com.example.thymeleafspringboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PeopleServiceImpl {

    @Autowired
    private PeopleRepository peopleRepository;

    public People getById(Long id){
        return peopleRepository.getById(id);
    }
    public void deletePeopleById(long id) {
        this.peopleRepository.deletePeopleById(id);
    }

    public Page<People> paginationGet(int pageNum, int size) {
        Pageable pageable = PageRequest.of(pageNum-1,size);
        return this.peopleRepository.findAll(pageable);
    }

}
